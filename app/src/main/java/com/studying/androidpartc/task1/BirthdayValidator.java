package com.studying.androidpartc.task1;

import android.content.Intent;
import android.util.Log;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class BirthdayValidator {
    private BirthdayValidator() {
    }

    public static boolean validate(EditText init) {
        if (init.getText() == null || init.getText().toString().isEmpty()) {
            return false;
        }

        String initStr = init.getText().toString();

        if (initStr.length() != 10) {
            return false;
        }

        if (initStr.charAt(2) != '/' || initStr.charAt(5) != '/') {
            return false;
        }

        String[] dateStrings = initStr.split("/");

        GregorianCalendar realTime = new GregorianCalendar();
        realTime.setTime(new Date());

        int inDay = Integer.parseInt(dateStrings[0]);
        int inMonth = Integer.parseInt(dateStrings[1]);
        int inYear = Integer.parseInt(dateStrings[2]);

        if (inYear > realTime.get(Calendar.YEAR)) {
            return false;
        }

        if (inYear == realTime.get(Calendar.YEAR) &&
                (inMonth > realTime.get(Calendar.MONTH) +1)) {
            return false;
        }

        if (inYear == realTime.get(Calendar.YEAR) &&
                inMonth == realTime.get(Calendar.MONTH) + 1 &&
                inDay > realTime.get(Calendar.DAY_OF_MONTH)) {
            return false;
        }

        if (inMonth > 12) {
            return false;
        }

        if (inYear % 4 == 0 && inMonth == 2 && inDay > 29) {
            return false;
        } else if (!(inYear % 4 == 0) && inMonth == 2 && inDay > 28) {
            return false;
        }

        if (isBigMonth(inMonth) && inDay > 31) {
            return false;
        }
        if (!isBigMonth(inMonth) && inDay > 30) {
            return false;
        }

        return true;
    }

    private static boolean isBigMonth(Integer month) {
        List<Integer> bigMonths = new ArrayList<>();
        bigMonths.add(1);
        bigMonths.add(3);
        bigMonths.add(5);
        bigMonths.add(7);
        bigMonths.add(8);
        bigMonths.add(10);
        bigMonths.add(12);
        return bigMonths.contains(month);
    }
}
