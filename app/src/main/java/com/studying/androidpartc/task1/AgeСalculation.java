package com.studying.androidpartc.task1;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class AgeСalculation {
    private String birthdayStr;

    private int days;
    private int months;
    private int years;

    public int getDays() {
        return days;
    }

    public int getMonths() {
        return months;
    }

    public int getYears() {
        return years;
    }

    public AgeСalculation(String birthdayStr) {
        this.birthdayStr = birthdayStr;
        calculate();
    }

    private void calculate() {
        String[] inStrings = birthdayStr.split("/");

        int inDay = Integer.parseInt(inStrings[0]);
        int inMonth = Integer.parseInt(inStrings[1]);
        int inYear = Integer.parseInt(inStrings[2]);

        Calendar inTime = new GregorianCalendar();
        inTime.set(inYear, inMonth, inDay);

        Calendar realTime = new GregorianCalendar();
        realTime.setTime(new Date());

        years = realTime.get(Calendar.YEAR) - inYear;
        months = years * 12 + (realTime.get(Calendar.MONTH) + 1 - inTime.get(Calendar.MONTH));
        days = calculateDays(inTime, realTime);
    }

    private int calculateDays(Calendar inTime, Calendar realTime) {
        return (int) ((realTime.getTimeInMillis() - inTime.getTimeInMillis()) / 1000 / 60 / 60 / 24 + 30);
    }

    private int leapYearsLived(int oldYear, int highYear) {
        int count = 0;
        for (int i = oldYear; i < highYear; i++) {
            if (i % 4 == 0) {
                count++;
            }
        }
        return count;
    }


}
