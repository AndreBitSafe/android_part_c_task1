package com.studying.androidpartc.task1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.studying.androidpartc.R;

public class DataActivity extends AppCompatActivity {

    private EditText name;
    private EditText birthday;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = findViewById(R.id.name_text);
        birthday = findViewById(R.id.birthday_text);
    }


    public void onButContinueClick(View view) {
        Intent intent = new Intent(getApplicationContext(), StatisticsActivity.class);
        if (!name.getText().toString().isEmpty() && BirthdayValidator.validate(birthday)) {
            intent.putExtra(StatisticsActivity.EXTRA_NAME, name.getText().toString());
            intent.putExtra(StatisticsActivity.EXTRA_BIRTHDAY, birthday.getText().toString());
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), "Ошибка!", Toast.LENGTH_SHORT).show();
        }

    }
}
