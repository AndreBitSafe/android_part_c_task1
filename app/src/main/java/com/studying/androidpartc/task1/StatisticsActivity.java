package com.studying.androidpartc.task1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.studying.androidpartc.R;

public class StatisticsActivity extends AppCompatActivity {

    public static final String EXTRA_NAME = ".task1.DataActivity.extra_name";
    public static final String EXTRA_BIRTHDAY = ".task1.DataActivity.extra_birthday";

    private TextView personNameView;
    private TextView yearsView;
    private TextView monthsView;
    private TextView daysView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        Intent intent = getIntent();
        String personName = intent.getStringExtra(EXTRA_NAME);
        String birthday = intent.getStringExtra(EXTRA_BIRTHDAY);

        personNameView = findViewById(R.id.person_name_view);
        yearsView = findViewById(R.id.years_view);
        monthsView = findViewById(R.id.months_view);
        daysView = findViewById(R.id.days_view);

        personNameView.setText(personName);

        AgeСalculation ageСalculation = new AgeСalculation(birthday);
        yearsView.setText(String.format("%s %d",
                getResources().getText(R.string.years),
                ageСalculation.getYears()));

        monthsView.setText(String.format("%s %d",
                getResources().getText(R.string.months),
                ageСalculation.getMonths()));

        daysView.setText(String.format("%s %d",
                getResources().getText(R.string.days),
                ageСalculation.getDays()));

    }

}
